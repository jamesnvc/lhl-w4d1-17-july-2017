//
//  Film.h
//  GhibliApiDemo
//
//  Created by James Cash on 17-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Film : NSObject

@property (nonatomic,strong) NSString* apiID;
@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSString* summary;
@property (nonatomic,strong) NSString* director;

- (instancetype)initWithInfo:(NSDictionary*)info;

@end
