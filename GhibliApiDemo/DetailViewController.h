//
//  DetailViewController.h
//  GhibliApiDemo
//
//  Created by James Cash on 17-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Film.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Film *detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

