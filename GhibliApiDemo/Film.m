//
//  Film.m
//  GhibliApiDemo
//
//  Created by James Cash on 17-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Film.h"

@implementation Film

- (instancetype)initWithInfo:(NSDictionary *)info
{
    if (self = [super init]) {
        _apiID = info[@"id"];
        _title = info[@"title"];
        _summary = info[@"description"];
        _director = info[@"director"];
    }
    return self;
}

@end
