//
//  MasterViewController.m
//  GhibliApiDemo
//
//  Created by James Cash on 17-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "Film.h"

@interface MasterViewController ()

@property NSArray<Film*> *films;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.films = @[];

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://ghibliapi.herokuapp.com/films"]];
    // if we wanted to make a request other than GET or set the body of our request, we could do that here

    NSURLSessionTask *task =
    [[NSURLSession sharedSession]
     // in this case, we probably just want a dataTaskWithURL since we aren't actually changing the request object
     dataTaskWithRequest:request
     completionHandler:^(NSData* data, NSURLResponse*  response, NSError * error) {
         NSLog(@"request finished");
         if (error != nil) {
             NSLog(@"Error making request: %@", error.localizedDescription);
             abort();
         }
//         NSLog(@"response data: %@", data);
         NSError *err = nil;
         // IN this case, we know from the api documentation that the result will be an array - but it could be something else for different APIs (e.g. commonly a dictionary with an array in one of the keys) - you need to read the docs to know what exactly to do here
         NSArray *results = [NSJSONSerialization JSONObjectWithData:data
                                                            options:0
                                                              error:&err];
         if (err != nil) {
             NSLog(@"error parsing JSON data: %@", err.localizedDescription);
             abort();
         }
         // this is a temporary array we're using to build up the list of Film objects from the results (which is an array of dictionaries)
         NSMutableArray *tmpFilms = [@[] mutableCopy];
         for (NSDictionary *info in results) {
             Film *film = [[Film alloc] initWithInfo:info];
             [tmpFilms addObject:film];
         }
         self.films = [NSArray arrayWithArray:tmpFilms];
         // because this is happening in the background, we now need to let the main thread know that it's time to update the display
         [[NSOperationQueue mainQueue] addOperationWithBlock:^{
             [self.tableView reloadData];
         }];
     }];
    // don't forget to start the request task that you made!
    [task resume];
}


- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        id object = self.films[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.films.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Film* film = self.films[indexPath.row];
    cell.textLabel.text = film.title;
    return cell;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

@end
